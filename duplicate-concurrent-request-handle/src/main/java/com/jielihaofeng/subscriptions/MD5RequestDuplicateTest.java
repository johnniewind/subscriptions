package com.jielihaofeng.subscriptions;

import com.jielihaofeng.subscriptions.util.RequestDuplicateHelper;

/**
 * @description MD5摘要算法请求去重测试
 * @author Johnnie Wind
 * @date 2020/12/9 00:00
 */
public class MD5RequestDuplicateTest {

    public static void main(String[] args) {
        // 两个请求参数除了时间差一秒，其他完全相同
        String reqParam1 = "{\n" +
                "    \"reqTime\":\"20201209120001\",\n" +
                "    \"key1\":\"val1\",\n" +
                "    \"key2\":\"val2\"\n" +
                "}";
        String reqParam2 = "{\n" +
                "    \"reqTime\":\"20201209120002\",\n" +
                "    \"key1\":\"val1\",\n" +
                "    \"key2\":\"val2\"\n" +
                "}";

        // 剔除时间因子前的全参数对比
        String md5DecreptParam1 = RequestDuplicateHelper.reqParamMD5(reqParam1, null);
        String md5DecreptParam2 = RequestDuplicateHelper.reqParamMD5(reqParam2, null);
        // req1MD5 = 8812666DE8CD2B64DD11446D8245616A , req2MD5=BBBE06778984BA973465F3CC5D4D6BB4
        System.out.println("req1MD5 = " + md5DecreptParam1 + " , req2MD5=" + md5DecreptParam2);
        // 结果MD5不同

        // 剔除时间因子，传时间因子字段 reqTime
        String md5DecreptParam11 = RequestDuplicateHelper.reqParamMD5(reqParam1, "reqTime");
        String md5DecreptParam22 = RequestDuplicateHelper.reqParamMD5(reqParam2, "reqTime");
        // req1MD5 = 53455EF321BC54595546DB7FAC2942AA , req2MD5=53455EF321BC54595546DB7FAC2942AA
        System.out.println("req1MD5 = " + md5DecreptParam11 + " , req2MD5=" + md5DecreptParam22);
        // MD5相同
    }

}
