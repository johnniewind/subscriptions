package com.jielihaofeng.subscriptions;

import com.jielihaofeng.subscriptions.util.RequestDuplicateHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisStringCommands;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.types.Expiration;

/**
 * @description Redis处理核心代码
 * @author Johnnie Wind
 * @date 2020/12/7 23:38
 */
public class RedisHandleDemo {

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    /**
     * @description 利用唯一请求编号去重
     * @author Johnnie Wind
     * @date 2020/12/9 0:14
     * @Param []
     * @return void
     */
    public void handle() {
        // 请求唯一编号
        String key = "WCX20201207520";
        // Redis过期时间：1000毫秒，1000ms内再次请求被认为是重复请求
        long expireTime = 1000L;
        // 过期时间戳
        long expireAt = System.currentTimeMillis() + expireTime;
        // 过期时间值
        String value = "expireAt@" + expireAt;
        // 设值、查询Redis的值
        Boolean firstSet = stringRedisTemplate.execute((RedisCallback<Boolean>) connection -> connection.set(key.getBytes(), value.getBytes(), Expiration.milliseconds(expireTime), RedisStringCommands.SetOption.SET_IF_ABSENT));

        final boolean isConsiderDup;
        // 第一次访问
        if (firstSet != null && firstSet) {
            isConsiderDup = false;
        } else { // redis值已存在，认为是重复了
            isConsiderDup = true;
        }

        // 输出结果
        System.out.println("此次访问是否重复->"+ isConsiderDup);

        // do some thing awesome...
        // 做你爱做的事

    }


    /**
     * @description 请求参数去重
     * @author Johnnie Wind
     * @date 2020/12/9 0:15
     * @Param []
     * @return void
     */
    public void paramMD5Handle() {
        String reqParam1 = "{\n" +
                "    \"reqTime\":\"20201209120001\",\n" +
                "    \"key1\":\"val1\",\n" +
                "    \"key2\":\"val2\"\n" +
                "}";
        // 用户ID
        String userId = "jielihaofeng";
        // 接口名
        String method = "syncCurrencyData";
        // 参数摘要，剔除时间干扰
        String md5DecreptParam1 = RequestDuplicateHelper.reqParamMD5(reqParam1, null);
        // Redis缓存唯一标识
        String KEY = "dedup:U=" + userId + "M=" + method + "P=" + md5DecreptParam1;
        // Redis过期时间：1000毫秒，1000ms内再次请求被认为是重复请求
        long expireTime = 1000L;
        // 过期时间戳
        long expireAt = System.currentTimeMillis() + expireTime;
        // 过期时间值
        String value = "expireAt@" + expireAt;
        // 设值、查询Redis的值
        Boolean firstSet = stringRedisTemplate.execute((RedisCallback<Boolean>) connection -> connection.set(KEY.getBytes(), value.getBytes(), Expiration.milliseconds(expireTime),
                RedisStringCommands.SetOption.SET_IF_ABSENT));

        final boolean isConsiderDup;
        // 第一次访问
        if (firstSet != null && firstSet) {
            isConsiderDup = false;
        } else { // redis值已存在，认为是重复了
            isConsiderDup = true;
        }

        // 输出结果
        System.out.println("此次访问是否重复->"+ isConsiderDup);

        // do some thing awesome...
        // 做你爱做的事

    }


}
