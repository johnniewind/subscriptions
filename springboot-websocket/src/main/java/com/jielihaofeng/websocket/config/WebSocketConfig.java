package com.jielihaofeng.websocket.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * @description 开启WebSocket支持
 * @author Johnnie Wind
 * @date 2021/1/11 17:41
 */
@Configuration
public class WebSocketConfig {
    /**
     * @description 自动注册使用@ServerEndpoint注解声明的websocket endpoint
     * @author Johnnie Wind
     * @date 2021/1/12 9:53
     * @Param []
     * @return org.springframework.web.socket.server.standard.ServerEndpointExporter
     */
    @Bean
    public ServerEndpointExporter serverEndpointExporter(){
        return new ServerEndpointExporter();
    }
}
